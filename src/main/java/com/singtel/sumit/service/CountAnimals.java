package com.singtel.sumit.service;

import java.util.Arrays;

import com.singtel.sumit.behaviour.impl.fly.CanFly;
import com.singtel.sumit.behaviour.impl.sing.CanSing;
import com.singtel.sumit.behaviour.impl.swim.CanSwim;
import com.singtel.sumit.behaviour.impl.walk.CanWalk;
import com.singtel.sumit.model.Parent;


public class CountAnimals {
	
	public int flyableCount( Parent species[]){

        return Arrays.stream( species ).filter( animal -> animal.getFlyable() instanceof CanFly ).toArray().length;

    }

    public int walkableCount( Parent species[]){

        return Arrays.stream( species ).filter( animal -> animal.getWalkable() instanceof CanWalk ).toArray().length;

    }

    public int singCount( Parent species[]){

        return Arrays.stream( species ).filter( animal -> animal.getSingable() instanceof CanSing ).toArray().length;

    }

    public int swimableCount( Parent species[]){

        return Arrays.stream( species ).filter( animal -> animal.getSwimable() instanceof CanSwim ).toArray().length;

    }

}
