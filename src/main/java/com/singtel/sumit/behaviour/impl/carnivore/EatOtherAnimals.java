package com.singtel.sumit.behaviour.impl.carnivore;

import com.singtel.sumit.behaviour.Carnivore;

public class EatOtherAnimals implements Carnivore {


    public void eatOtherAnimals() {
        System.out.println( "Eating Other Animals" );
    }
}
