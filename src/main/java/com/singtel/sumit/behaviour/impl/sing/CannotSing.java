package com.singtel.sumit.behaviour.impl.sing;

import com.singtel.sumit.behaviour.Singable;

public class CannotSing implements Singable {

	public void sing() {
        System.out.println("Can not sing");
    }
}
