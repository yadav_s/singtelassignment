package com.singtel.sumit.behaviour.impl.carnivore;

public class CannotEatOtherFishes extends CannotEatOtherAnimals {

    public void eatOtherAnimals() {
        System.out.println( "Cannot eat Other Fishes" );
    }
}
