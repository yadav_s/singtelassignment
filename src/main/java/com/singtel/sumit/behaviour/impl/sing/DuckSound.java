package com.singtel.sumit.behaviour.impl.sing;

public class DuckSound extends CanSing {

    @Override
    public void sing() {
        System.out.println("Quack,quack");
    }
}
