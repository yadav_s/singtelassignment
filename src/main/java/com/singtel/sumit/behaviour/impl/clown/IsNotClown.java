package com.singtel.sumit.behaviour.impl.clown;

import com.singtel.sumit.behaviour.Clown;

public class IsNotClown implements Clown {

    public void makeJokes() {
        System.out.println( "Cannot make jokes" );
    }
}
