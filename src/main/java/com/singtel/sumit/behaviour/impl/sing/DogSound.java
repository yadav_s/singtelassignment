package com.singtel.sumit.behaviour.impl.sing;

public class DogSound extends CanSing {

    @Override
    public void sing() {
        System.out.println("Woof,woof");
    }
}
