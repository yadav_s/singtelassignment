package com.singtel.sumit.behaviour.impl.walk;

import com.singtel.sumit.behaviour.Walkable;

public class CannotWalk implements Walkable {

    public void walk() {
        System.out.println( "Cannot Walk" );
    }
}
