package com.singtel.sumit.behaviour.impl.walk;

import com.singtel.sumit.behaviour.Walkable;

public class CanWalk implements Walkable {

    public void walk() {
        System.out.println( "I am walking" );
    }
}
