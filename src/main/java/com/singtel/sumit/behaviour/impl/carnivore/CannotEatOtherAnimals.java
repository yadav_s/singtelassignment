package com.singtel.sumit.behaviour.impl.carnivore;

import com.singtel.sumit.behaviour.Carnivore;

public class CannotEatOtherAnimals implements Carnivore {

    public void eatOtherAnimals() {
        System.out.println( "Cannot eat Other Animals" );
    }
}
