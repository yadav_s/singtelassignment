package com.singtel.sumit.behaviour.impl.swim;

import com.singtel.sumit.behaviour.Swimable;

public class CanSwim implements Swimable {
	public void swim() {
        System.out.println("I am swimming");
    }
}
