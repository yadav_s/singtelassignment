package com.singtel.sumit.behaviour.impl.clown;

import com.singtel.sumit.behaviour.Clown;

public class IsClown implements Clown {

    public void makeJokes() {
        System.out.println( "Can make Jokes");
    }
}
