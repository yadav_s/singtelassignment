package com.singtel.sumit.behaviour.impl.sing;

import com.singtel.sumit.behaviour.Singable;

public class CanSing implements Singable {

	public void sing() {
        System.out.println("I am singing");
    }
}
