package com.singtel.sumit.behaviour.impl.fly;

import com.singtel.sumit.behaviour.Flyable;

public class CannotFly implements Flyable {

	public void fly() {
		System.out.println("I am flying");
	}
}
