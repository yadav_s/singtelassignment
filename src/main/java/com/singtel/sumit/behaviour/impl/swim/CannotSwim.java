package com.singtel.sumit.behaviour.impl.swim;

import com.singtel.sumit.behaviour.Swimable;

public class CannotSwim implements Swimable  {
	public void swim() {
        System.out.println("Cannot Swim");
    }
}
