package com.singtel.sumit.behaviour;

public interface Carnivore {
	public void eatOtherAnimals();
}
