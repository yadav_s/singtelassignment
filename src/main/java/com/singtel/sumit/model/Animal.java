package com.singtel.sumit.model;

import com.singtel.sumit.behaviour.impl.walk.CanWalk;

public class Animal extends Parent {

    public Animal(){
        this.walkable=new CanWalk();
    }
}