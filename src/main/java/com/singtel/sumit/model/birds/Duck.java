package com.singtel.sumit.model.birds;

import com.singtel.sumit.behaviour.impl.sing.DuckSound;
import com.singtel.sumit.behaviour.impl.swim.CanSwim;
import com.singtel.sumit.model.Bird;

public class Duck extends Bird {

    public Duck() {

        this.swimable = new CanSwim();
        this.singable = new DuckSound();
    }
}
