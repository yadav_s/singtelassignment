package com.singtel.sumit.model.birds;

import com.singtel.sumit.behaviour.impl.fly.CannotFly;
import com.singtel.sumit.behaviour.impl.sing.ChickenSound;
import com.singtel.sumit.model.Bird;

public class Chicken extends Bird {

    public Chicken() {

        this.flyable = new CannotFly();
        this.singable = new ChickenSound();
    }
}
