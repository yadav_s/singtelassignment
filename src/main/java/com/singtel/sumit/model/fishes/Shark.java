package com.singtel.sumit.model.fishes;

import com.singtel.sumit.Features.Colour;
import com.singtel.sumit.Features.Size;
import com.singtel.sumit.behaviour.impl.carnivore.EatOtherFishes;
import com.singtel.sumit.model.Fish;

public class Shark extends Fish {

    public Shark(){
        this.size = Size.LARGE;
        this.colour = Colour.GREY;
        this.carnivore = new EatOtherFishes();
    }
}
