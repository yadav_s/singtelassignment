package com.singtel.sumit.model.fishes;

import com.singtel.sumit.Features.Colour;
import com.singtel.sumit.Features.Size;
import com.singtel.sumit.behaviour.impl.clown.IsClown;
import com.singtel.sumit.model.Fish;

public class Clownfish extends Fish {

    public Clownfish(){
        this.size = Size.SMALL;
        this.colour = Colour.ORANGE;
        this.clown = new IsClown();
    }
}
