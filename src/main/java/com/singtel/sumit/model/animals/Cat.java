package com.singtel.sumit.model.animals;

import com.singtel.sumit.behaviour.impl.sing.CatSound;
import com.singtel.sumit.model.Animal;

public class Cat extends Animal {

    public Cat(){
        this.singable = new CatSound();
    }
}
