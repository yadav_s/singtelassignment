package com.singtel.sumit.model.animals;

import com.singtel.sumit.behaviour.impl.fly.CannotFly;
import com.singtel.sumit.behaviour.impl.walk.Crawl;
import com.singtel.sumit.model.Animal;

public class Caterpillar extends Animal {

    public Caterpillar(){
        this.flyable = new CannotFly();
        this.walkable = new Crawl();
    }
}
