package com.singtel.sumit.model.animals;

import com.singtel.sumit.behaviour.impl.sing.DogSound;
import com.singtel.sumit.model.Animal;

public class Dog extends Animal {

    public Dog(){
        this.singable = new DogSound();
    }
}
