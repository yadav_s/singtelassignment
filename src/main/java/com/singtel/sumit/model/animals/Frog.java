package com.singtel.sumit.model.animals;

import com.singtel.sumit.behaviour.impl.sing.CanSing;
import com.singtel.sumit.model.Animal;

public class Frog extends Animal {

    public Frog(){
        this.singable = new CanSing();
    }
}
