package com.singtel.sumit.model;


import com.singtel.sumit.behaviour.impl.sing.CanSing;
import com.singtel.sumit.behaviour.impl.fly.CanFly;
import com.singtel.sumit.behaviour.impl.walk.CanWalk;
import com.singtel.sumit.model.Parent;

public class Bird extends Parent{

    public Bird(){
        this.walkable=new CanWalk();
        this.flyable =new CanFly();
        this.singable=new CanSing();
    }
}