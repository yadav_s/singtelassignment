package com.singtel.sumit.model;

import com.singtel.sumit.behaviour.Singable;
import com.singtel.sumit.behaviour.Swimable;
import com.singtel.sumit.behaviour.Clown;
import com.singtel.sumit.behaviour.Carnivore;
import com.singtel.sumit.behaviour.Flyable;
import com.singtel.sumit.behaviour.Walkable;

public class Parent {
	
	protected Walkable walkable;
    protected Flyable flyable;
    protected Singable singable;
    protected Swimable swimable;
    protected Carnivore carnivore;
    protected Clown clown;

    public Walkable getWalkable() {
        return walkable;
    }

    public void setWalkable(Walkable walkable) {
        this.walkable = walkable;
    }

    public Flyable getFlyable() {
        return flyable;
    }

    public void setFlyable(Flyable flyable) {
        this.flyable = flyable;
    }
    
    public Singable getSingable() {
        return singable;
    }

    public void setSingable(Singable singable) {
        this.singable = singable;
    }
    
    public Swimable getSwimable() {
        return swimable;
    }

    public void setSwimable(Swimable swimable) {
        this.swimable = swimable;
    }
    
    public Carnivore getCarnivore() {
        return carnivore;
    }

    public void setCarnivore(Carnivore carnivore) {
        this.carnivore = carnivore;
    }
    
    public Clown getClown() {
        return clown;
    }

    public void setClown(Clown clown) {
        this.clown = clown;
    }

}
