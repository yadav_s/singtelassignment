package com.singtel.sumit.model;

import com.singtel.sumit.Features.Colour;
import com.singtel.sumit.Features.Size;
import com.singtel.sumit.behaviour.impl.carnivore.CannotEatOtherFishes;
import com.singtel.sumit.behaviour.impl.clown.IsNotClown;
import com.singtel.sumit.behaviour.impl.sing.CannotSing;
import com.singtel.sumit.behaviour.impl.swim.CanSwim;
import com.singtel.sumit.behaviour.impl.walk.CannotWalk;

public class Fish  extends Parent{

	protected Size size;
    protected Colour colour;


    public Fish(){
        this.walkable = new CannotWalk();
        this.singable = new CannotSing();
        this.swimable = new CanSwim();
        this.carnivore = new CannotEatOtherFishes();
        this.clown = new IsNotClown();
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public Colour getColour() {
        return colour;
    }

    public void setColour(Colour colour) {
        this.colour = colour;
    }
}